from receipts.views import (
    AccountCreateView,
    AccountListView,
    ExpenseCategoryCreateView,
    ExpenseListView,
    ReceiptCreateView,
    ReceiptListView,
)


from django.urls import path


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("create/", ReceiptCreateView.as_view(), name="create_receipt"),
    path(
        "categories/", ExpenseListView.as_view(), name="expense_category_list"
    ),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="create_category",
    ),
    path(
        "accounts/create/", AccountCreateView.as_view(), name="create_account"
    ),
]
